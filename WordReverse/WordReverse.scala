class WordReverse {
  def reverse(input: Array[String]): List[String] = {
    return input.toList.reverse
  }
}

object WordReverse {
  def main(args: Array[String]) {
    println(args.length)
    val wr = new WordReverse
    wr.reverse(args).foreach(arg => print(arg + " "))
    print("\n")
  }
}